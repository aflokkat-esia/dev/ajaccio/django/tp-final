from django.contrib.auth import authenticate
from django.contrib.auth import login as djangoLogin
from django.contrib.auth import logout as djangoLogout
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group, User
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect, render

from .forms import *
from .models import *


@login_required(login_url="/login")
def home(request: HttpRequest) -> HttpResponse:
    return render(request, 'app/pages/home.html')


@login_required(login_url="/login")
def entreprises(request: HttpRequest) -> HttpResponse:
    entreprises = Entreprise.objects.all()
    context = { "entreprises": entreprises }
    return render(request, 'app/pages/entreprises.html', context)


@login_required(login_url="/login")
def clients(request: HttpRequest) -> HttpResponse:
    clients = Client.objects.all()
    context = { "clients": clients }
    return render(request, 'app/pages/clients.html', context)


@login_required(login_url="/login")
def reclamations(request: HttpRequest) -> HttpResponse:
    reclamations = Reclamation.objects.all()
    context = { "reclamations": reclamations }
    return render(request, 'app/pages/reclamations.html', context)


@login_required(login_url="/login")
def entreprise(request: HttpRequest, pk: int) -> HttpResponse:
    entreprise = Entreprise.objects.get(id=pk)
    context = { "entreprise": entreprise }
    return render(request, 'app/pages/entreprise.html', context)


@login_required(login_url="/login")
def client(request: HttpRequest, pk: int) -> HttpResponse:
    client = Client.objects.get(id=pk)
    context = { "client": client }
    return render(request, 'app/pages/client.html', context)


@login_required(login_url="/login")
def reclamation(request: HttpRequest, pk: int) -> HttpResponse:
    reclamation = Reclamation.objects.get(id=pk)
    context = { "reclamation": reclamation }
    return render(request, 'app/pages/reclamation.html', context)


@login_required(login_url="/login")
def create_entreprise(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = EntrepriseForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('entreprises')
    form = EntrepriseForm()
    context = { "form": form }
    return render(request, 'app/pages/create-entreprise.html', context)


@login_required(login_url="/login")
def create_client(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = ClientForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('clients')
    form = ClientForm()
    context = { "form": form }
    return render(request, 'app/pages/create-client.html', context)


@login_required(login_url="/login")
def create_reclamation(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = ReclamationForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('reclamations')
    form = ReclamationForm()
    context = { "form": form }
    return render(request, 'app/pages/create-reclamation.html', context)


@login_required(login_url="/login")
def update_entreprise(request: HttpRequest, pk: int) -> HttpResponse:
    entreprise = Entreprise.objects.get(id=pk)
    if request.method == "POST":
        form = EntrepriseForm(request.POST, instance=entreprise)
        if form.is_valid:
            form.save()
            return redirect('entreprises')
    form = EntrepriseForm(instance=entreprise)
    context = { "form": form }
    return render(request, 'app/pages/update-entreprise.html', context)


@login_required(login_url="/login")
def update_client(request: HttpRequest, pk: int) -> HttpResponse:
    client = Client.objects.get(id=pk)
    if request.method == "POST":
        form = ClientForm(request.POST, instance=client)
        if form.is_valid:
            form.save()
            return redirect('clients')
    form = ClientForm(instance=client)
    context = { "form": form }
    return render(request, 'app/pages/update-client.html', context)


@login_required(login_url="/login")
def update_reclamation(request: HttpRequest, pk: int) -> HttpResponse:
    reclamation = Reclamation.objects.get(id=pk)
    if request.method == "POST":
        form = ReclamationForm(request.POST, instance=reclamation)
        if form.is_valid:
            form.save()
            return redirect('reclamations')
    form = ReclamationForm(instance=reclamation)
    context = { "form": form }
    return render(request, 'app/pages/update-reclamation.html', context)


@login_required(login_url="/login")
def delete_entreprise(request: HttpRequest, pk: int) -> HttpResponse:
    entreprise = Entreprise.objects.get(id=pk)
    entreprise.delete()
    return redirect('entreprises')


@login_required(login_url="/login")
def delete_client(request: HttpRequest, pk: int) -> HttpResponse:
    client = Client.objects.get(id=pk)
    client.delete()
    return redirect('clients')


@login_required(login_url="/login")
def delete_reclamation(request: HttpRequest, pk: int) -> HttpResponse:
    reclamation = Reclamation.objects.get(id=pk)
    reclamation.delete()
    return redirect('reclamations')


@login_required(login_url="/login")
def validate_reclamation(request: HttpRequest, pk: int) -> HttpResponse:
    reclamation = Reclamation.objects.get(id=pk)
    reclamation.traite = True
    reclamation.save()
    return redirect('reclamations')


@login_required(login_url="/login")
def devalidate_reclamation(request: HttpRequest, pk: int) -> HttpResponse:
    reclamation = Reclamation.objects.get(id=pk)
    reclamation.traite = False
    reclamation.save()
    return redirect('reclamations')


def login(request: HttpRequest) -> HttpResponse:
    if request.user.is_authenticated:
        return redirect('home')
    
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        try:
            user = User.objects.get(username=username)
        except:
            return redirect('login')
        
        user = authenticate(request, username=username, password=password)

        if user is None:
            return redirect('login')
        else:
            djangoLogin(request, user)
            return redirect('home')
        
    return render(request, 'app/pages/login.html')


def register(request: HttpRequest) -> HttpResponse:
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == "POST":
        form = UserCreationForm(request.POST)
        
        if form.is_valid:
            user = form.save()
            group = Group.objects.get(name="BasicUser")
            user.groups.add(group)
            return redirect('login')
    form = UserCreationForm()
    context = { "form": form }
    return render(request, 'app/pages/register.html', context)


@login_required(login_url="login")
def logout(request: HttpRequest) -> HttpResponse:
    djangoLogout(request)
    return redirect('login')
